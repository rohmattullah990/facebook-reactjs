import React, {Component} from 'react'
import "./style.css"
class Header extends Component{
    constructor(props){
        super(props);
        this.state = {
            userName : "",
            password : ""
        }
    }

    handelChange = (e) =>{
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handelSubmit = (e) =>{
        console.log("ok")
        if (this.state.userName === "admin" && this.state.password === "12345")
            this.props.isLoggedIn()
        else 
            alert('username atau password salah')
    }

    render(){
        return(
            <div id="header">
                <div id="logo">
                    <h1>facebook</h1>
                </div>
                <div id="login">
                    <table>
                        <tr><td>Email or Phone</td>
                        <td colspan="2">Password</td>
                        </tr>
                        <tr>
                            <td><input type="text" name="userName" className="text" onChange={this.handelChange}/></td>
                            <td><input type="password" name="password" className="text" onChange={this.handelChange}/></td>
                            <td><input type="submit" className="submit" value="Log In" onClick={this.handelSubmit}/></td>
                            
                        </tr>
                        <tr>
                            <td><input type="checkbox"/>keep me logged in</td> 
                            <td colspan="2"><a class="forgot" href="#">forgot your pasword ?</a> </td>
                        </tr>
                    </table>
                </div>
            </div>
        );
    }
}

export default Header;