import Header from "./header"
import Footer from "./footer"
import Nav from "./navigation"
import Body from './body'

export {Header, Footer, Nav, Body}