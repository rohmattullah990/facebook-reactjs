import React, { Component } from 'react';
import {Home, About, Login} from '../pages'

class Body extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            text: "ini kosong"
         }
    }
    render() { 
        const{ page, goTo, changeLoginStatus} = this.props
        console.log(page)
        if (page === "login")
            return <Login goTo={goTo} changeLoginStatus={changeLoginStatus}/>
        else if (page === "home")
            return <Home/>
        else if (page === "about")
            return <About/>
        return ( 
            <div>
                Default Page
            </div>
         );
    }
}
 
export default Body;