import React, {Component} from 'react'
import Menu from '../menu'
class Nav extends Component{
    constructor(props){
      super(props);
      this.state = {}
    }
    render(){
      return(
        <div style={{
            display : "flex",
            justifyContent : "flex-end"
        }} >
          <Menu onClickMenu={() => this.props.goTo("home")}>Home</Menu>
          <Menu onClickMenu={() => this.props.goTo("about")}>About</Menu>
          <Menu onClickMenu={() => this.props.goTo("login")}>Login</Menu>
        </div>
      )  
    }
  }
  
  export default Nav;