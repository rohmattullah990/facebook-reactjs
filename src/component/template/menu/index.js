import React, {Component} from 'react'

class Menu extends Component{
    constructor(props){
         super(props);
        this.state={}
    }

    render(){
        return(
            <div style={{
                padding: "10px 20px",
                margin:"10px ",
                border: "1px solid grey"
            }} onClick= {() => this.props.onClickMenu()}>

                {this.props.children}
            </div>
        );
    }
}

export default Menu;