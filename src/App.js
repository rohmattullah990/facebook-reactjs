import React, { Component } from 'react';
// import{Header, Footer, Nav, Body} from './component/template'
import{Header, Content, Footer, Home, Login} from './facebook/template'

class App extends Component{
  constructor(props){
    super(props);
    this.state = {isLoggedIn : false}
  }



  changeLoginStatus = () => {
    this.setState({
      isLoggedIn: !this.state.isLoggedIn
    })
  }
  render(){
    const logIn = this.state.isLoggedIn ? 
      <Home isLoggedIn={this.changeLoginStatus}/> 
      : 
      <>
        <Content/>
        <Footer/>
      </>
    return(
      <div>
        {/* <Header/>
        <Nav
          goTo={this.changePage}
          changeLoginStatus={this.changeLoginStatus}
          isLoggedIn={this.state.isLoggedIn}
        />
        <Body
          goTo={this.changePage}
          page={this.state.page}

        />
        <Footer/> */}
        
        <Header isLoggedIn={this.changeLoginStatus}/>
        {logIn}
        
      </div>
    )  
  }
}

export default App;
